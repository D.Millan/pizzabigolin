package pizzariabigolin;

/**
 *
 * @author Aluno
 */
public class Milho extends Pizza {

    public Milho() {
        addIngrediente(new Ingrediente("Milho", 5.0, "Faltando 5 minutos"));
        addIngrediente(new Ingrediente("Queijo", 1.0, "Sem lactose"));
        addIngrediente(new Ingrediente("Orégano", 10.0, "Da índia"));
        formularReceita();
        setNome("Milho");
    }
}
