package pizzariabigolin;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SortEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Aluno
 */
public class Tela2Controller implements Initializable {

    private Pizza pizzaSel;
    @FXML
    private TableView<Pizza> tabela;
    @FXML
    private TableColumn<?, ?> idT;
    @FXML
    private TableColumn<?, ?> nomeT;
    @FXML
    private TableColumn<?, ?> receitaT;
    @FXML
    private Button prontoB, cozinhandoB;
    @FXML
    private TableColumn<?, ?> statusT;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        addItems();
        makeColumns();
    }

    @FXML
    private void addItems() {
        try {
            tabela.getItems().clear();
            for (Pizza p : Pizza.getNaoProntos()) {
                tabela.getItems().add(p);
            }
        } catch (Exception e) {
            System.out.println("aa");
        }
    }

    private void makeColumns() {
        idT.setCellValueFactory(new PropertyValueFactory<>("id"));//precisa ter um getNome    
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome"));//precisa ter um getNome    
        receitaT.setCellValueFactory(new PropertyValueFactory<>("receita"));//precisa ter um getNome
        statusT.setCellValueFactory(new PropertyValueFactory<>("status"));//precisa ter um getNome
    }

    @FXML
    private void selecionar(MouseEvent event) {
        pizzaSel = tabela.getSelectionModel().getSelectedItem();
        if (pizzaSel != null) {
            if (!pizzaSel.load()) {
                addItems();
            } else if ("Em espera".equals(pizzaSel.getStatus())) {
                pizzaSel.cozinhaPizza();
            }
            tabela.refresh();

            if ("Cozinhando".equals(pizzaSel.getStatus())) {
                prontoB.setDisable(false);
            } else {
                prontoB.setDisable(true);
            }
        } else {
            prontoB.setDisable(true);
        }
    }

    @FXML
    private void pronto(ActionEvent event) {
        pizzaSel.terminaPizza();
        tabela.getItems().remove(pizzaSel);
        tabela.refresh();
        prontoB.setDisable(true);
    }

    @FXML
    private void voltar(ActionEvent event) {
        Main.trocaTela("Tela1.fxml");
    }

}
