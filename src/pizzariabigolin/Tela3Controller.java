package pizzariabigolin;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

/**
 *
 * @author Aluno
 */
public class Tela3Controller implements Initializable {

    @FXML
    private TableView<Pizza> tabela;
    @FXML
    private TableColumn<?, ?> idT;
    @FXML
    private TableColumn<?, ?> clienteT;
    @FXML
    private TableColumn<?, ?> nomeT;
    @FXML
    private TableColumn<?, ?> statusT;
    @FXML
    private TextField clienteA;
    @FXML
    private Button entregueB;

    private Pizza pizzaSel;

    @FXML
    private void addItems() {
        try {
            tabela.getItems().clear();

            for (Pizza p : Pizza.getAll()) {
                tabela.getItems().add(p);
            }
        } catch (Exception e) {
            System.out.println("aa");
        }
    }

    private void makeColumns() {
        idT.setCellValueFactory(new PropertyValueFactory<>("id"));//precisa ter um getNome    
        clienteT.setCellValueFactory(new PropertyValueFactory<>("cliente"));//precisa ter um getNome    
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome"));//precisa ter um getNome    
        statusT.setCellValueFactory(new PropertyValueFactory<>("status"));//precisa ter um getNome

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        addItems();
        makeColumns();
    }

    @FXML
    private void voltar(ActionEvent event) {
        Main.trocaTela("Tela1.fxml");
    }

    @FXML
    private void selecionar(MouseEvent event) {
        pizzaSel = tabela.getSelectionModel().getSelectedItem();
        if (pizzaSel != null) {
            if (pizzaSel.load()) {
                addItems();
            }
            tabela.refresh();
            if ("Em espera".equals(pizzaSel.getStatus())) {
                pizzaSel.delete();
                tabela.getItems().remove(pizzaSel);
                tabela.refresh();
            }

            if ("Pronta".equals(pizzaSel.getStatus())) {
                entregueB.setDisable(false);
            } else {
                entregueB.setDisable(true);
            }
        } else {
            entregueB.setDisable(true);
        }
    }

    @FXML
    private void entregue(ActionEvent event) {
        pizzaSel.entregaPizza();
        tabela.refresh();
        entregueB.setDisable(true);
    }

    @FXML
    private void criarPizzaC(ActionEvent event) {
        Pizza novo = new Calabresa();
        novo.setCliente(clienteA.getText());
        novo.setStatus(0);
        if (novo.insert()) {
            addItems();
        }
    }

    @FXML
    private void criarPizzaMi(ActionEvent event) {
        Pizza novo = new Milho();
        novo.setCliente(clienteA.getText());
        novo.setStatus(0);
        if (novo.insert()) {
            addItems();
        }
    }

    @FXML
    private void criarPizzaMa(ActionEvent event) {
        Pizza novo = new Marguerita();
        novo.setCliente(clienteA.getText());
        novo.setStatus(0);
        if (novo.insert()) {
            addItems();
        }

        System.out.println(novo.getNome());
    }

    @FXML
    private void clean() {
        Pizza.clean();
        addItems();
    }

}
