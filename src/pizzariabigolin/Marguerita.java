package pizzariabigolin;

/**
 *
 * @author Aluno
 */
public class Marguerita extends Pizza {

    public Marguerita() {
        addIngrediente(new Ingrediente("Tomate", 5.0, "Fresco"));
        addIngrediente(new Ingrediente("Queijo", 1.0, "Sem lactose"));
        addIngrediente(new Ingrediente("Orégano", 10.0, "Da índia"));
        formularReceita();
        setNome("Marguerita");

    }
}
