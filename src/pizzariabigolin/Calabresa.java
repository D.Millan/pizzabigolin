package pizzariabigolin;

/**
 *
 * @author Aluno
 */
public class Calabresa extends Pizza {

    public Calabresa() {
        addIngrediente(new Ingrediente("Calabresa", 5.0, "Fresco"));
        addIngrediente(new Ingrediente("Queijo", 1.0, "Sem lactose"));
        addIngrediente(new Ingrediente("Orégano", 10.0, "Da índia"));
        formularReceita("Assar por mais 10 minutos");
        setNome("Calabresa");
    }

}
