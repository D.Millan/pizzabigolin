package pizzariabigolin;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author Aluno
 */
public class Tela1Controller implements Initializable {

    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button button1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void cozTela(ActionEvent event) {
        Main.trocaTela("Tela2.fxml");
    }

    @FXML
    private void ateTela(ActionEvent event) {
        Main.trocaTela("Tela3.fxml");
    }

}
