/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzariabigolin;

/**
 *
 * @author Aluno
 */
public class Ingrediente {

    private String nome, obs;
    private Double qtd;

    public Ingrediente(String nome, Double qtd, String obs) {
        this.nome = nome;
        this.obs = obs;
        this.qtd = qtd;
    }

    @Override
    public String toString() {
        return "Adicione " + nome + ", qtd=" + qtd + " (" + obs + ')';
    }

}
