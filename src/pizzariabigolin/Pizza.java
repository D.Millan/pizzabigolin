package pizzariabigolin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aluno
 */
public class Pizza {

    private String cliente, nome, receita = "";
    private int id, status;
    private ArrayList<Ingrediente> ing = new ArrayList();

    public void formularReceita(String obs) {
        formularReceita();
        receita += obs;
    }

    public void formularReceita() {
        for (Ingrediente i : ing) {
            receita += i;
            receita += "; ";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public static void clean() {
        try {
            Statement st = Conexao.getConexao().createStatement();
            st.execute("DELETE PIZZA WHERE(STATUS=3)");
        } catch (Exception ex) {
            System.out.println("aaaaaa");
        }
    }

    public String getStatus() {
        if (status == 0) {
            return "Em espera";
        }

        if (status == 1) {
            return "Cozinhando";
        }

        if (status == 2) {
            return "Pronta";
        }

        return "Entregue";
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean insert() {
        try {
            //Statement stmt = Conexao.getConexao().createStatement();
            //ResultSet rs = stmt.executeQuery("SELECT pedidos.NEXTVAL FROM dual in 1");
            //if (rs.next()) {
            //    id = rs.getInt(1);
            //}
            PreparedStatement ps = Conexao.getConexao().prepareStatement("INSERT INTO PIZZA(id, nome, cliente, status) VALUES (pedidos.NEXTVAL,?,?, ?)");
            ps.setString(1, this.nome);
            ps.setString(2, this.cliente);
            ps.setInt(3, 0);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("7" + e.getMessage());
            return false;
        } finally {
            Conexao.desconecta();
        }
    }

    public boolean cozinhaPizza() {
        String sql = "UPDATE pizza SET status = ? WHERE (id = ?)";
        this.status = 1;

        try {
            PreparedStatement ps = Conexao.getConexao().prepareStatement(sql);
            ps.setInt(1, 1);
            ps.setInt(2, this.id);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("1");
            e.printStackTrace();
        } finally {
            Conexao.desconecta();
        }
        return true;
    }

    public boolean terminaPizza() {
        String sql = "UPDATE pizza SET status = ? WHERE (id = ?)";
        this.status = 2;
        try {
            PreparedStatement ps = Conexao.getConexao().prepareStatement(sql);
            ps.setInt(1, 2);
            ps.setInt(2, this.id);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("2");
            e.printStackTrace();
        } finally {
            Conexao.desconecta();
        }
        return true;
    }

    public boolean entregaPizza() {
        String sql = "UPDATE pizza SET status = ? WHERE (id = ?)";
        this.status = 3;
        try {
            PreparedStatement ps = Conexao.getConexao().prepareStatement(sql);
            ps.setInt(1, 3);
            ps.setInt(2, this.id);
            ps.executeUpdate();

        } catch (Exception e) {
            System.out.println("9");
            e.printStackTrace();
        } finally {
            Conexao.desconecta();
        }
        return true;
    }

    public boolean delete() {
        try {
            PreparedStatement ps = Conexao.getConexao().prepareStatement("delete pizza where id = ?");
            ps.setInt(1, this.id);
            ps.executeQuery();
            return true;
        } catch (Exception e) {
            System.out.println("3");
            return false;
        } finally {
            Conexao.desconecta();
        }
    }

    public boolean load() {
        try { //ps
            Statement st = Conexao.getConexao().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM PIZZA WHERE(id = " + this.id + ") ORDER BY ID");
            if (rs.next()) {
                setId(rs.getInt("id"));
                setCliente(rs.getString("cliente"));
                setStatus(rs.getInt("status"));
                return true;
            }

        } catch (Exception e) {
            System.out.println("4");

        } finally {
            Conexao.desconecta();
        }
        return false;
    }

    public static List<Pizza> getNaoProntos() {
        List pizzas = new ArrayList();
        try {
            Statement st = Conexao.getConexao().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM PIZZA WHERE(STATUS<2)ORDER BY ID");

            while (rs.next()) {
                Pizza novo;
                if ("Milho".equals(rs.getString("nome"))) {
                    novo = new Milho();

                } else if ("Marguerita".equals(rs.getString("nome"))) {
                    novo = new Marguerita();
                } else {
                    novo = new Calabresa();
                }

                novo.setId(rs.getInt("id"));
                novo.setCliente(rs.getString("cliente"));
                novo.setStatus(rs.getInt("status"));
                pizzas.add(novo);
            }
        } catch (Exception e) {
            System.out.println("5");
        } finally {
            Conexao.desconecta();
        }
        return pizzas;
    }

    public static List<Pizza> getAll() {
        List pizzas = new ArrayList();
        try {
            Statement st = Conexao.getConexao().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM PIZZA ORDER BY ID");

            while (rs.next()) {
                Pizza novo;
                if ("Milho".equals(rs.getString("nome"))) {
                    novo = new Milho();

                } else if ("Marguerita".equals(rs.getString("nome"))) {
                    novo = new Marguerita();
                } else {
                    novo = new Calabresa();
                }

                novo.setId(rs.getInt("id"));
                novo.setCliente(rs.getString("cliente"));
                novo.setStatus(rs.getInt("status"));
                pizzas.add(novo);
            }
        } catch (Exception e) {
            System.out.println("6");
        } finally {
            Conexao.desconecta();
        }
        return pizzas;

    }

    public String getReceita() {
        return receita;
    }

    public void addIngrediente(Ingrediente i) {
        ing.add(i);
    }

    public String getCliente() {

        return cliente;
    }

    public String getNome() {
        return nome;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

}
