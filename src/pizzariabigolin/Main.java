package pizzariabigolin;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Aluno
 */
public class Main extends Application {

    private static Stage stage;
    private ArrayList<Pizza> pedidos = new ArrayList<>();

    public ArrayList getPedidos() {
        return this.pedidos;
    }

    public static void trocaTela(String tela) {
        try {
            Parent root = FXMLLoader.load(Main.class.getResource(tela));
            Scene scene = new Scene(root);
            Main.stage.setScene(scene);
            Main.stage.show();
        } catch (Exception e) {
            System.out.println("erro " + e.getMessage());
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Tela1.fxml"));
        Scene scene = new Scene(root);
        Main.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
